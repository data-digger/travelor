from dataclasses import replace
from doctest import FAIL_FAST
from tkinter.tix import Tree
from importlib_metadata import NullFinder
import sentry_sdk
from flask import Flask, url_for, redirect, url_for, escape, render_template, request, session
from flask_cors import CORS
from sentry_sdk.integrations.flask import FlaskIntegration
import json
import random

from rapidminer_connect import *

sentry_sdk.init(
    dsn="https://examplePublicKey@o0.ingest.sentry.io/0",
    integrations=[FlaskIntegration()],

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,

    # By default the SDK will try to use the SENTRY_RELEASE
    # environment variable, or infer a git commit
    # SHA as release, however you may want to set
    # something more human-readable.
    # release="myapp@1.0.0",
)

FLASK_DEBUG = 1
app = Flask(__name__)
app.secret_key = "TBA.V2"
CORS(app, resources={r'/*': {'origins': '*'}})


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/question', methods=["POST", "GET"])
def question():

    with open('database/questionnaire.json', 'r', encoding="utf8") as f:
        data = json.load(f)

    if request.method == "GET":
        return render_template('question.html', question=data[0])
    elif request.method == "POST":
        get_answer = int(request.form["question-number"])
        session[request.form["question-keyword"]
                ] = request.form["answer-selected"]

        if get_answer == 20:
            writeNewSample(session['Season'],      session['Companion'],       session['Group_size'],
                           session['Length'],      session['Accommodation'],   session['Avg_spending'],
                           session['Environment'], session['Location_type'],   session['Activity'],
                           session['Food'],        session['Weather'],         session['Quiet'],
                           session['Cost'],        session['Convenience'],     session['Nature'],
                           session['Scenery'],     session['Culture'],         session['Variety'],
                           session['Vibe'],        session['Photogenic'])

            return redirect(url_for("loading"))
        else:
            return render_template('question.html', question=data[get_answer])
    else:
        return render_template('index.html')


@app.route('/loading')
def loading():
    return render_template('loading.html')


@app.route('/process')
def process():
    if False:
        applyModel()
    session['Prediction'] = getPrediction()
    session['MaxConfidence'] = getMaxConfidence()

    return redirect(url_for("result"))


@app.route('/result')
def result():
    with open('database/places.json', 'r', encoding="utf8") as f:
        data = json.load(f)

    region = session['Prediction']
    place = random.sample(data[0][region], 3)
    return render_template('result.html', zone=region, percent=session['MaxConfidence'],  location=place)


@app.route('/debug-sentry')
def trigger_error():
    division_by_zero = 1 / 0


if __name__ == "__main__":
    app.run(host="localhost", port=80, debug=True)
