import rapidminer
import pandas

def rmConnect():
    # connect to the local RapidMiner Studio
    connection = rapidminer.Studio('C:/Program Files/RapidMiner/RapidMiner Studio')
    return connection

def writeNewSample(Season, Companion, Group_size, Length, Accommodation, Avg_spending, Environment, Location_type, Activity, Food, Weather, Quiet, Cost, Convenience, Nature, Scenery, Culture, Variety, Vibe, Photogenic):
    # header of the csv file
    header = ['Season', 'Companion', 'Length', 'Accommodation', 'Group_size', 'Avg_spending', 'Environment', 'Location_type', 'Activity', 'Food', 'Weather', 'Quiet', 'Cost', 'Convenience',	'Nature', 'Scenery', 'Culture',	'Variety',	'Vibe',	'Photogenic']
    # retrieve input data
    data = [[Season, Companion, Length, Accommodation, Group_size, Avg_spending, Environment, Location_type, Activity, Food, Weather, Quiet, Cost, Convenience, Nature, Scenery, Culture, Variety, Vibe, Photogenic]]
    # combine header with user data to make a table
    newSample = pandas.DataFrame(data, columns=header)
    # export as csv using pandas
    newSample.to_csv('./temp/newSample.csv', index=False)

def applyModel():
    # execute the rf_prediction process
    df = connector.run_process('//Local Repository/processes/rf_prediction')

def getPrediction():
    csvfile = pandas.read_csv('./temp/result.csv')
    prediction = csvfile.iloc[:, 26].values[0]
    
    if prediction ==  'Central':
        return 'ภาคกลาง'
    elif prediction ==  'Eastern':
        return 'ภาคตะวันออก'
    elif prediction ==  'West':
        return 'ภาคตะวันตก'
    elif prediction ==  'Southern':
        return 'ภาคใต้'
    elif prediction ==  'Northeast':
        return 'ภาคตะวันออกเฉียงเหนือ'
    elif prediction ==  'Northern':
        return 'ภาคเหนือ'
    else:
        return "N/A"

def getMaxConfidence():
    csvfile = pandas.read_csv('./temp/result.csv')
    confidence = csvfile.iloc[:, [20, 21, 22, 23, 24, 25]].values.max()
    return str(round(confidence*100, 2  ))+'%'

# connect to the local RapidMiner Studio
connector = rmConnect()

# main
'''
writeNewSample(Season, Companion, Group_size, Length, Accommodation, Avg_spending, Environment, Location_type, Activity, Food, Weather, Quiet, Cost, Convenience, Nature, Scenery, Culture, Variety, Vibe, Photogenic)
applyModel()
region = getPrediction()
confidence = getMaxConfidence()

'''
# output test
#print(region+' '+confidence)